Dicer
====

Roll dice on your Android phone.

Feature Requests
===
=> [click here](https://gitlab.com/kas70/Dicer/issues/new)

Building
====
Should work with
```
git clone --recursive https://gitlab.com/kas70/Dicer.git
cd Dicer/
./gradlew build
```

License
====
Dicer is released under the terms of [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).
