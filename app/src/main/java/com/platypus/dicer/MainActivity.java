/*
* Copyright (C) 2014 kas70
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package com.platypus.dicer;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toolbar;


public class MainActivity extends ActionBarActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String PREFS = "PrefsFile";
    protected int ndies = 1;
    protected boolean shaketoroll = true;
    protected boolean vibrate = true;
    protected boolean playsound = true;

    protected boolean dice1active = true;
    protected boolean dice2active = true;
    protected boolean dice3active = true;
    protected boolean dice4active = true;
    protected boolean dice5active = true;


    protected boolean showDice = true;

    private ShakeListener shali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //remove title (is there a better way?)
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS, 0);
        ndies = settings.getInt("ndies", 1);
        shaketoroll = settings.getBoolean("shake", true);
        playsound = settings.getBoolean("playsound", true);
        vibrate = settings.getBoolean("vibrate", true);

        //PreferenceChangeListener
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.registerOnSharedPreferenceChangeListener(this);

        //set proper layout (1, 2 or 5 dice)
        setLayout();

        //set onTouchListeners
        setImageListeners();

        //set ShakeListener
        if (!shaketoroll) setShakeListener();
    }

    protected void onStop() {
        if (shaketoroll) {
            shali.pause();
        }
        super.onStop();
        //store preferences
        SharedPreferences settings = getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("ndies", ndies);
        editor.putBoolean("shake", shaketoroll);
        editor.putBoolean("playsound", playsound);
        editor.putBoolean("vibrate", vibrate);
        editor.commit();
    }

    @Override
    protected void onResume() {
        setLayout();
        setShakeListener();
        super.onResume();
    }

    protected void setShakeListener() {
        if (shaketoroll) {
            shali = new ShakeListener(this);
            shali.setOnShakeListener(new ShakeListener.OnShakeListener() {
                @Override
                public void onShake() {
                    final ImageView v = (ImageView) findViewById(R.id.dice);
                    final ImageView vtwo = (ImageView) findViewById(R.id.dice2);

                    if (ndies == 1) {

                        switch (rollDice()) {
                            case 1:
                                v.setImageResource(R.drawable.dice1);
                                break;
                            case 2:
                                v.setImageResource(R.drawable.dice2);
                                break;
                            case 3:
                                v.setImageResource(R.drawable.dice3);
                                break;
                            case 4:
                                v.setImageResource(R.drawable.dice4);
                                break;
                            case 5:
                                v.setImageResource(R.drawable.dice5);
                                break;
                            case 6:
                                v.setImageResource(R.drawable.dice6);
                                break;
                        }
                    } else {
                        if(dice2active) {
                            switch (rollDice()) {
                                case 1:
                                    vtwo.setImageResource(R.drawable.dice1);
                                    break;
                                case 2:
                                    vtwo.setImageResource(R.drawable.dice2);
                                    break;
                                case 3:
                                    vtwo.setImageResource(R.drawable.dice3);
                                    break;
                                case 4:
                                    vtwo.setImageResource(R.drawable.dice4);
                                    break;
                                case 5:
                                    vtwo.setImageResource(R.drawable.dice5);
                                    break;
                                case 6:
                                    vtwo.setImageResource(R.drawable.dice6);
                                    break;
                            }
                        }
                        if(dice1active) {
                            switch (rollDice()) {
                                case 1:
                                    v.setImageResource(R.drawable.dice1);
                                    break;
                                case 2:
                                    v.setImageResource(R.drawable.dice2);
                                    break;
                                case 3:
                                    v.setImageResource(R.drawable.dice3);
                                    break;
                                case 4:
                                    v.setImageResource(R.drawable.dice4);
                                    break;
                                case 5:
                                    v.setImageResource(R.drawable.dice5);
                                    break;
                                case 6:
                                    v.setImageResource(R.drawable.dice6);
                                    break;
                            }
                        }
                        playDieSound();
                        vibrate();
                    }
                    playDieSound();
                    vibrate();
                }
            });
        }

    }

    protected void playDieSound() {
        if(playsound) {
            SoundPool sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
            int soundId = sp.load(getApplicationContext(), R.raw.dicesound, 1); // in 2nd param u have to pass your desire ringtone

            sp.play(soundId, 1, 1, 0, 0, 1);

            MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.dicesound); // in 2nd param u have to pass your desire ringtone
            mPlayer.start();
        }
    }

    protected void vibrate() {
        if (vibrate) {
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(70);
        }
    }

    public void setLayout() {
        if (ndies == 1) {
            setContentView(R.layout.activity_main);
        } else if (ndies == 2) {
            setContentView(R.layout.activity_main_twodies);
            // todo: outsource this part
            final Button button = (Button) findViewById(R.id.hide_button);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (showDice) {
                        showDice = false;
                        ImageView image = (ImageView) findViewById(R.id.dice);
                        image.setVisibility(View.INVISIBLE);
                        image = (ImageView) findViewById(R.id.dice2);
                        image.setVisibility(View.INVISIBLE);
                        button.setText(R.string.button_show);
                        final Button roll_button = (Button) findViewById(R.id.roll_button);
                        roll_button.setVisibility(View.VISIBLE);
                        roll_button.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                final ImageView dice1 = (ImageView) findViewById(R.id.dice);
                                final ImageView dice2 = (ImageView) findViewById(R.id.dice2);
                                if(dice1active) shuffleImage(dice1);
                                if(dice2active) shuffleImage(dice2);
                            }
                        });
                    } else {
                        showDice = true;
                        ImageView image = (ImageView) findViewById(R.id.dice);
                        image.setVisibility(View.VISIBLE);

                        image = (ImageView) findViewById(R.id.dice2);
                        image.setVisibility(View.VISIBLE);

                        button.setText(R.string.button_hide);

                        final Button roll_button = (Button) findViewById(R.id.roll_button);
                        roll_button.setVisibility(View.INVISIBLE);

                    }
                }
            });
        }
        // 5 dies
        else{
            setContentView(R.layout.activity_main_fivedies);
        }
        //because image listeners won't work, if layout changes
        setImageListeners();
    }

    protected void setImageListeners() {

        final ImageView v = (ImageView) findViewById(R.id.dice);
        final ImageView vtwo = (ImageView) findViewById(R.id.dice2);
        final ImageView vthree = (ImageView) findViewById(R.id.dice3);
        final ImageView vfour = (ImageView) findViewById(R.id.dice4);
        final ImageView vfive = (ImageView) findViewById(R.id.dice5);

        if (ndies == 1) {
            v.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    shuffleImage(v);
                    return false;
                }
            });
        }
        else if (ndies == 2){
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if(dice1active){
                        v.setAlpha((float) 0.5);
                        dice1active = false;
                    }
                    else{
                        v.setAlpha((float) 1);
                        dice1active = true;
                    }
                    return true;
                }
            });
            v.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (dice1active) shuffleImage(v);
                    if (dice2active) shuffleImage(vtwo);
                }
            });

            vtwo.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (dice2active) {
                        vtwo.setAlpha((float) 0.5);
                        dice2active = false;
                    } else {
                        vtwo.setAlpha((float) 1);
                        dice2active = true;
                    }
                    return true;
                }
            });
            vtwo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (dice1active) shuffleImage(v);
                    if (dice2active) shuffleImage(vtwo);
                }
            });

        }
        // 5 dies
        else {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shuffleAll(v, vtwo, vthree, vfour, vfive);
                }
            });
            vtwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shuffleAll(v, vtwo, vthree, vfour, vfive);
                }
            });
            vthree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shuffleAll(v, vtwo, vthree, vfour, vfive);
                }
            });
            vfour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shuffleAll(v, vtwo, vthree, vfour, vfive);
                }
            });
            vfive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shuffleAll(v, vtwo, vthree, vfour, vfive);
                }
            });

            // todo: disable/enable dice on long click
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return false;
                }
            });
            vtwo.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return false;
                }
            });
            vthree.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return false;
                }
            });
            vfour.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return false;
                }
            });
            vfive.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return false;
                }
            });
        }
    }


    protected void shuffleImage(ImageView v) {
        playDieSound();
        vibrate();
        switch (rollDice()) {
            case 1:
                v.setImageResource(R.drawable.dice1);
                break;
            case 2:
                v.setImageResource(R.drawable.dice2);
                break;
            case 3:
                v.setImageResource(R.drawable.dice3);
                break;
            case 4:
                v.setImageResource(R.drawable.dice4);
                break;
            case 5:
                v.setImageResource(R.drawable.dice5);
                break;
            case 6:
                v.setImageResource(R.drawable.dice6);
                break;
        }
    }
    protected void shuffleAll(ImageView v, ImageView vtwo, ImageView vthree, ImageView vfour, ImageView vfive){
        // only vibrate ones
        vibrate();
        boolean h = vibrate;
        vibrate = false;
        if(dice1active) shuffleImage(v);
        if(dice2active) shuffleImage(vtwo);
        if(dice3active) shuffleImage(vthree);
        if(dice4active) shuffleImage(vfour);
        if(dice5active) shuffleImage(vfive);
        vibrate = h;
    }

    // return random number between 1 and 6
    protected int rollDice() {
        return (int) (Math.random() * 6 + 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mf = getMenuInflater();
        mf.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.settings:
                openSettings(findViewById(R.id.settings));
                return true;
            case R.id.close:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openSettings(View view) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String string = sp.getString("PREF_NUMBEROFDIES", "none");
        if(string.equals("1")) ndies = 1;
        else if(string.equals("2")) ndies = 2;
        else ndies = 5;
        shaketoroll = sp.getBoolean("PREF_SHAKETOROLL", false);
        playsound = sp.getBoolean("PREF_PLAYSOUND", true);
        vibrate = sp.getBoolean("PREF_VIBRATE", true);
    }
}